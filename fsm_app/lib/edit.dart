import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'models/contact.dart';

class Edit extends StatelessWidget 
{
  const Edit({Key key, @required this.contact}) : super(key: key);

  final Contact contact;

  @override
  Widget build(BuildContext context) 
  {
    FlutterStatusbarcolor.setStatusBarColor(Colors.blue);

    return Scaffold(
      body: SingleChildScrollView(

          child: Container(
            margin: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
            child: Center(

              child: Column(
                children: <Widget>[
                  
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                      "Edit Contact",
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                          color: Colors.black
                      ),
                    ),
                  ),
                  
                  Stack(      
                    alignment: Alignment.center,          
                    children: [
                      Container(
                        width: 300,
                        height: 300,
                        // padding: EdgeInsets.all(value),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(200),
                          border: Border.all(width: 5.0, color: Colors.grey),
                        ),
                        child: Stack(
                          children: [
                            Container(
                              child: Icon(
                                Icons.person,
                                size: 290,
                                color: Colors.grey[400],            
                              ),
                            ),
                            Container(
                              alignment: Alignment.bottomRight,
                              margin: EdgeInsets.only(bottom: 30, right: 30),
                              child: Icon(
                                Icons.camera_alt,
                                size: 62,
                                color: Colors.grey[700],
                              ),
                            )
                          ],
                        )                    
                      ),
                      
                    ]
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20, horizontal: 60),
                    child: Row(
                      children: <Widget>[
                        Expanded(                    
                          flex: 2,
                          child: Text(
                            "Name:",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black
                            ),
                          ),
                        ),
                        
                        Expanded(                      
                          flex: 3,                      
                          child: TextField(   
                            controller: TextEditingController(text: contact.name),                     
                            style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.w900,
                            ),
                            // decoration: BoxDecoration(
                            //   color: Colors.black,
                            //   borderRadius: BorderRadius.circular(20),
                            //   border: Border.all(width: 2.0, color: Colors.black) 
                            // ),
                            decoration: InputDecoration(          
                              contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ), 
                              hintText: 'Name',
                              hintStyle: TextStyle(
                                color: Colors.grey[400]
                              ),
                            ),

                          ),
                        ),
                        
                      ],
                    ),              
                  ),
                  
                  Container(
                    margin: EdgeInsets.only(bottom:20, left: 60, right: 60),
                    child: Row(
                      children: <Widget>[
                        Expanded(                    
                          flex: 2,
                          child: Text(
                            "Title:",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black
                            ),
                          ),
                        ),
                        
                        Expanded(                      
                          flex: 3,                      
                          child: TextField(   
                            controller: TextEditingController(text: contact.description),                     
                            style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.w900,
                            ),
                            decoration: InputDecoration(          
                              contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ), 
                              hintText: 'Title',
                              hintStyle: TextStyle(
                                color: Colors.grey[400]
                              ),
                            ),

                          ),
                        ),
                        
                      ],
                    ),              
                  ),

                  Container(
                    margin: EdgeInsets.only(bottom:20, left: 60, right: 60),
                    child: Row(
                      children: <Widget>[
                        Expanded(                    
                          flex: 2,
                          child: Text(
                            "Company:",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black
                            ),
                          ),
                        ),
                        
                        Expanded(                      
                          flex: 3,                      
                          child: TextField(  
                            // controller: TextEditingController(text: contact.mobile),                      
                            style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.w900,
                            ),
                            decoration: InputDecoration(          
                              contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ), 
                              hintText: 'Company',
                              hintStyle: TextStyle(
                                color: Colors.grey[400]
                              ),
                            ),

                          ),
                        ),
                        
                      ],
                    ),              
                  ),

                  Divider(
                    height: 2.5,
                    color: Colors.black45,
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(vertical:20, horizontal: 60),
                    child: Row(
                      children: <Widget>[
                        Expanded(                    
                          flex: 2,
                          child: Text(
                            "Mobile:",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black
                            ),
                          ),
                        ),
                        
                        Expanded(                      
                          flex: 3,                      
                          child: TextField(
                            controller: TextEditingController(text: contact.mobile),
                            style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.w900,
                            ),
                            decoration: InputDecoration(          
                              contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide(
                                  color: Colors.black87,
                                  width: 2.0
                                )
                              ), 
                              hintText: 'Mobile',
                              hintStyle: TextStyle(
                                color: Colors.grey[400]
                              ),
                            ),

                          ),
                        ),
                        
                      ],
                    ),              
                  ),

                  SizedBox(
                    height: 40,
                  ),

                  FlatButton(                
                    color: Colors.blue,
                    textColor: Colors.white,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    // padding: EdgeInsets.all(8.0),
                    padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 100.0),
                    splashColor: Colors.blueAccent,
                    onPressed: () 
                    {
                      /*...*/
                    },
                    child: Text(
                      "Save",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  )

                ],
              ),
              
            ),      
          ),
        )
      );
  }
}