import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:fsm_app/details.dart';
import 'dart:ui';
import 'models/contact.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

void main() 
{
  runApp(MyApp());
}

class MyApp extends StatelessWidget 
{
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context)
   {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SearchBar',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity
      ),
      home: MyHomePage(title: 'Search Bar'),
    );
  }
}

class MyHomePage extends StatefulWidget 
{
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> 
{

  void addContact()
  {
    setState(() 
    {
      
    });
  }


  @override
  Widget build(BuildContext context) 
  {
    return Scaffold(
       appBar: AppBar(
        
        title: TextField(
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w900,
          ),
          decoration: InputDecoration(          
            contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(40),
              borderSide: BorderSide(
                color: Colors.white,
                width: 2.0
              )
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(40),
              borderSide: BorderSide(
                color: Colors.white,
                width: 2.0
              )
            ),
            disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(40),
              borderSide: BorderSide(
                color: Colors.white24,
                width: 2.0
              )
            ), 
            hintText: 'Search',
            hintStyle: TextStyle(
              color: Colors.grey[400]
            ),
          ),

        ),
       ),

      //  body: SingleChildScrollView(

        body: Container(
          width: double.infinity,
          //  padding: EdgeInsets.only(left: 50, right: 50),
          child: ListView(
            children: <Widget>[
              Container(
                child: tabs()
              )
            ],
          ),
        ),

      //  ),


       floatingActionButton: FloatingActionButton(
         onPressed: addContact,
         child: Icon(
           Icons.add
         ),
       ),

      //  bottomNavigationBar: BottomBar()
    );
  }
}


Widget tabs()
{
  return Container(
    height: 900,
    width: double.infinity,
    child: DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.blue,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(1),
            child: Container(
              color: Colors.blue,
              child: SafeArea(
                child: Column(
                  children: <Widget>[
                    TabBar(
                      isScrollable: false,
                      labelPadding: EdgeInsets.only(top: 15),
                      indicatorColor: Colors.blue[900],
                      labelColor: Colors.white,
                      labelStyle: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w800,
                        // fontFamily: "slabo"
                      ),
                      unselectedLabelColor: Colors.grey[300],
                      unselectedLabelStyle: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w400
                      ),
                      tabs: <Widget>[

                        Container(
                          child: Text("All"),
                        ),
                        Container(
                          child: Text("Favorite"),
                        ),

                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        body: TabBarView(
          
          children: <Widget>[

            allContactShowCase(),
            favoriteContactShowCase(),

            // Center(
            //   child: Text(
            //     "favorite Contact Show Case",
            //     textAlign: TextAlign.center,
            //     style: TextStyle(
            //       fontSize: 15
            //     ),
            //   ),
            // ),
            

          ],
        ),
      ),
    ),
  );
}

Widget allContactShowCase()
{
  return Container(

    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    child: ListView.builder(

      scrollDirection: Axis.vertical,
      itemCount: contactList.contacts.length,
      itemBuilder: (BuildContext context, int i)
      {
        return ListOfContacts(
          name: contactList.contacts[i].name,
          description: contactList.contacts[i].description,
          profilePhoto: contactList.contacts[i].profilePhoto,
          mobile: contactList.contacts[i].mobile,
          contactObject: contactList.contacts[i]
        );
      }
    ),

  );
}


class ListOfFavContacts extends StatefulWidget 
{
  ListOfFavContacts(
  {
    Key key,
    @required this.name,
    @required this.description,
    @required this.profilePhoto,
    @required this.mobile,
    @required this.contactObject

  }) : super(key: key);

  String name;
  String description;
  String profilePhoto;
  String mobile;
  Contact contactObject;

  @override
  _ListOfFavContactsState createState() => _ListOfFavContactsState();
}

class _ListOfFavContactsState extends State<ListOfFavContacts> 
{
  Contact contactObject;

@override
  void initState()
  {
    super.initState();
    contactObject = widget.contactObject;
  }

  @override
  Widget build(BuildContext context) 
  {
    return Column(

      children: <Widget>[

        GestureDetector(
          onTap: ()
          {

          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            margin: EdgeInsets.only(top: 10, bottom: 10),
            height: 150,
            width: double.infinity,

            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(width: 2.0, color: Colors.blue),
            ),

            child: new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[                  
                  new Row(
                    children: <Widget>[

                      contactAvatar(contactObject),
                      ContactText(
                        name: contactObject.name,
                        desc: contactObject.description
                      ),
                      StatefullFavIcon(mContact: contactObject),
                      // new Container(
                      //   alignment: Alignment.topRight,
                      //   child: new Text("0000")
                      // )

                    ],
                  )
                ],
              ),
            )

          ),
        )
      
      ],
    
    );
  }
}



class ListOfContacts extends StatelessWidget 
{
  const ListOfContacts({
    Key key,
    @required this.name,
    @required this.description,
    @required this.profilePhoto,
    @required this.mobile,
    @required this.contactObject,
    
  }) : super(key: key);

  final String name;
  final String description;
  final String profilePhoto;
  final String mobile;
  final Contact contactObject;

  @override
  Widget build(BuildContext context) 
  {
    return Column(

      children: <Widget>[

        GestureDetector(
          onTap: ()
          {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => Details(contactObject: contactObject)
            ));
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            margin: EdgeInsets.only(top: 10, bottom: 10),
            height: 150,
            width: double.infinity,

            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(width: 2.0, color: Colors.blue),
            ),

            child: new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[                  
                  new Row(
                    children: <Widget>[

                      // contactAvatar(contactObject),
                      // ContactText(
                      //   name: contactObject.name,
                      //   desc: contactObject.description
                      // ),
                      // StatefullFavIcon(mContact: contactObject),

                      Expanded(
                        flex: 2,
                        child: contactAvatar(contactObject),
                      ),
                      Expanded(
                        flex: 4,
                        child: ContactText(
                          name: contactObject.name,
                          desc: contactObject.description
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: StatefullFavIcon(mContact: contactObject),
                      )
                    
                    ],
                  )
                ],
              ),
            )

          ),
        )
      
      ],
    
    );
  }
}

class ContactText extends StatelessWidget 
{
  const ContactText({ 
    @required this.name,
    @required this.desc
  });

  final String name;
  final String desc;

  @override
  Widget build(BuildContext context) 
  {
    return Column(          
      children: <Widget>[      
        new Container(
          margin: EdgeInsets.only(left: 30),
          width: 320,
          child: Text(
            "$name",
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 20,
                color: Colors.black
            ),
          ),
        ),

        new SizedBox(height: 20),

        new Container(
          margin: EdgeInsets.only(left: 30),
          width: 320,
          child: Text(
            "$desc",
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18,
                color: Colors.grey
            ),
          ),
        ),     
      ],
    );
  }
}

Widget contactAvatar(Contact contact)
{
  return Container(
    width: 80,
    height: 80,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(80),
      border: Border.all(width: 2.5, color: Colors.grey),
    ),
    child: new Icon(
    Icons.person,
      size: 70.0,
      color: Colors.grey[400],
    ),

  );
  // return new Icon(
  //   Icons.person,
  //   size: 100.0,
  //   color: Colors.grey[600],
  // );
}

class StatefullFavIcon extends StatefulWidget 
{
  StatefullFavIcon({Key key, @required this.mContact }) : super(key: key);

  final Contact mContact;

  @override
  _StatefullFavIconState createState() => _StatefullFavIconState();
}

class _StatefullFavIconState extends State<StatefullFavIcon> 
{
  bool isFav;

  @override
  void initState() 
  {
    super.initState();

    isFav = widget.mContact.isFav;    
  }

  @override
  Widget build(BuildContext context) 
  {
    return GestureDetector(
      onTap: ()
      {
        setState(() 
        {
          isFav = !isFav;
          widget.mContact.isFav = isFav;
          if (isFav)
          {
            favorideList.favList.add(widget.mContact);
          }
          else
          {
            try
            {
              favorideList.favList.remove(widget.mContact);
            }
            catch(e)
            {

            }
          }
        });
      },    
      child: Icon(
        isFav ? Icons.star : Icons.star_border,
        color: Colors.blue,
        size: 48.0,
      ),
    );
  }
}





Widget favoriteContactShowCase()
{
  return Container(

    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    child: ListView.builder(

      scrollDirection: Axis.vertical,
      itemCount: favorideList.favList.length,
      itemBuilder: (BuildContext context, int i)
      {
        return ListOfContacts(
          name: favorideList.favList[i].name,
          description: favorideList.favList[i].description,
          profilePhoto: favorideList.favList[i].profilePhoto,
          mobile: favorideList.favList[i].mobile,
          contactObject: favorideList.favList[i]
        );
      }
    ),

  );
}




class BottomBar extends StatelessWidget 
{
  const BottomBar({Key key}) : super(key: key);

  final double _size = 35;
  final double _padding = 17;
  final double _margin = 17;

  @override
  Widget build(BuildContext context) 
  {
    return BottomAppBar(
      color: Colors.transparent,
      elevation: 2.0,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

            ],            
          ),
        ),        
      ),

    );
  }
}