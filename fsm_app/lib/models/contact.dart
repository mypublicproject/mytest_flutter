import 'package:flutter/material.dart';

ContactList contactList = ContactList(contacts: [

  new Contact(
    id: "0",
    name: "Javad Abadi",
    description: "Android Developer",
    mobile: "09121234567",
    profilePhoto: "",
    isFav: false
  ),

  Contact(
    id: "1",
    name: "MyName",
    description: "Flutter Developer",
    profilePhoto: "",
    mobile: "09129876543",
    isFav: false
  ),

  Contact(
    id: "2",
    name: "Javad JAM",
    description: "Admin",
    profilePhoto: "",
    mobile: "09351234567",
    isFav: false
  ),

] 
);

class ContactList 
{
  List<Contact> contacts;

  ContactList({@required this.contacts});
}

FavorideList favorideList = new FavorideList(favList: [

]);

class FavorideList
{
  List<Contact> favList = [];

  FavorideList({@required this.favList});
}

class Contact
{
  String id;
  String name;
  String description;
  String profilePhoto;
  String mobile;
  bool isFav;

  Contact({
    @required this.id,
    @required this.name,
    @required this.description,
    @required this.profilePhoto,
    @required this.mobile,
    @required this.isFav
  });
}