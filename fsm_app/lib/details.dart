import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:fsm_app/edit.dart';
import 'models/contact.dart';

class Details extends StatelessWidget 
{
  const Details({Key key, this.contactObject}) : super(key: key);

  final Contact contactObject;


  @override
  Widget build(BuildContext context) 
  {
    FlutterStatusbarcolor.setStatusBarColor(Colors.blue);

    return Scaffold(
      body: SingleChildScrollView(

          child: Container(
            margin: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: ()
                    {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => Edit(contact: contactObject)
                      ));
                    },
                    child: Icon(
                      Icons.edit,
                      size: 36.0,
                      color: Colors.black,                          
                    ),            
                  ),
                ),

                Container(
                  width: 300,
                  height: 300,
                  // padding: EdgeInsets.all(value),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(200),
                    border: Border.all(width: 5.0, color: Colors.grey),
                  ),
                  child: Icon(
                    Icons.person,
                    size: 290,
                    color: Colors.grey[400],            
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    "${contactObject.name}",
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                        color: Colors.black
                    ),
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    "${contactObject.description}",
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                        color: Colors.grey[700]
                    ),
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Divider(              
                    height: 2.0,
                    color: Colors.grey[900],
                  )
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 20, horizontal: 60),
                  child: Row(
                    children: <Widget>[
                      Expanded(                    
                        flex: 2,
                        child: Text(
                          "Mobile:",
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black
                          ),
                        ),
                      ),
                      
                      Expanded(
                        flex: 3,
                        child: Text(
                          "${contactObject.mobile}",
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                            color: Colors.black
                          ),
                        ),
                      ),
                      
                    ],
                  ),              
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    children: <Widget>[

                      Expanded(
                        flex: 2,
                        child: SizedBox(width: 20,),
                      ),
                      Expanded(
                        flex: 1,
                        child: Icon(
                          Icons.phone,
                          size: 48,                      
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Icon(
                          Icons.message,
                          size: 48,                      
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: SizedBox(width: 20,),
                      ),

                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Divider(              
                    height: 2.0,
                    color: Colors.grey[900],
                  )
                ),
                
              ],
            )
          ),
        )
      );
  }
}